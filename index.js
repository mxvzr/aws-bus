var AWS = require('aws-sdk'),
	assert = require('assert');

function Message(bus, msg) {
	if (!this instanceof Message)
		return new Message(bus, msg);

	var deleted = false;
	msg.Body = JSON.parse(msg.Body);
	Object.defineProperty('Message', this, { enumerable: true, value: msg.Body.Message });
	Object.defineProperty('Subject', this, { enumerable: true, value: msg.Body.Subject });
	Object.defineProperty('delete', this, { value: function() {
		if (!deleted) return;
		bus.delete(msg.ReceiptHandle);
		deleted = true;
	});
	Object.defineProperty('_msg', this, { value: msg });
}

function is_thenable(_) {
	return _ && typeof _.then === 'function';
}

function constructWorker(stack) {
	return function worker(msg, i) {
		var _;
		for(i = i || 0; i < stack.length; i++) {
			if (!stack[i][0](msg)) continue;
			_ = stack[i][1](msg);
			if (is_thenable(_)) {
				if (++i < stack.length)
					_ = _.then(function() { return worker(msg, i); });
				return _;
			}
		}
		return _;
	};
}

function receiveMessages(n, callback) {
	var self = this;
	this.sqsClient.receiveMessage({
		QueueUrl: this.sqsQueueUrl,
		MaxNumberOfMessages: Math.min(n, this.sqsMaxNumberOfMessages),
		WaitTimeSeconds: this.sqsWaitTimeSeconds
	}, function(err, response) {
		if (err) {
			self.emit('error', err);
		} else {
			var messages = response.Messages.forEach(function(msg) { return new Message(self, msg); });
			self.emit('messages', messages);
		}
		if (self.pollDelay)
			setTimeout(callback, self.pollDelay);
		else
			callback();
	});
}


function Bus(opts) {
	var self = this;
	opts = opts || {};

	assert(opts.snsTopicArn || opts.sqsQueueUrl, 'Required: "snsTopicArn" and/or "sqsQueueUrl"');

	Object.defineProperty('stack', this, {
		value: []
	});

	Object.defineProperty('sqsClient', this, {
		value: new AWS.SQS(opts.sqsOpts)
	});

	Object.defineProperty('snsClient', this, {
		value: new AWS.SNS(opts.snsOpts)
	});

	Object.defineProperty('snsTopicArn', this, {
		value: opts.snsTopicArn
	});

	Object.defineProperty('sqsQueueUrl', self, {
		value: opts.sqsQueueUrl
	});

	Object.defineProperty('pollDelay', this, {
		value: opts.pollDelay || 0
	});

	Object.defineProperty('defaultTick', this, {
		value: opts.defaultTick || 500
	});

	Object.defineProperty('sqsMaxNumberOfMessages', this, {
		value: opts.sqsMaxNumberOfMessages || 10
	});

	Object.defineProperty('sqsWaitTimeSeconds', this, {
		value: opts.sqsWaitTimeSeconds || 20
	});

	Object.defineProperty('maxConcurrency', this, {
		value: opts.maxConcurrency || this.sqsMaxNumberOfMessages
	});

	var i = 0;
	this.on('messages', function(messages) {
		if (!messages.length) return;
		i += messages.length;
		var jobs = self.work(messages);
		jobs.forEach(function(_) {
			if (is_thenable(_))
				_.then(function() { i--; }).catch(function(err) { i--; self.emit(err); });
			else
				i--;
		});
	});

	Object.defineProperty('length', this, {
		get: function() { return i; }
	});
}

Bus.prototype.use = function(path, fn) {
	if (!fn) fn = path, path = undefined;
	if (path instanceof RegExp)
		path = R.compose(R.test(path), R.path(['Message', 'Subject']));
	else if (typeof path === 'string')
		path = R.pathEq(['Message', 'Subject'], path);
	else if (!path)
		path = R.T;

	assert(typeof path === 'function');
	assert(typeof fn === 'function');
	this.stack.push([path, fn]);
	return this;
};

Bus.prototype.delete = function(receiptHandle) {
	var self = this;
	this.sqsClient.deleteMessage({
		QueueUrl: this.sqsQueueUrl,
		ReceiptHandle: receiptHandle
	}, function(err) {
		if (err) self.emit('error', err);
	});
};

Bus.prototype.start = function() {
	assert(this.sqsQueueUrl, 'Missing sqsQueueUrl option');
	var self = this,
		polling = false,
		cb = function() { polling = false; };

	this.work = R.map(constructWorker(this.stack));
	this.interval = setInterval(function() {
		if (!polling && self.length() < self.maxConcurrency) {
			polling = true;
			receiveMessages.bind(self)(self.maxConcurrency - self.length(), cb);
		}
	}, this.defaultTick);
};

Bus.prototype.stop = function() {
	clearInterval(this.interval);
	this.interval = undefined;
};

Bus.prototype.notify = function(subject, body) {
	assert(this.snsTopicArn, 'Missing snsTopicArn option');
	var self = this;
	this.snsClient.publish({
		Message: JSON.stringify(body),
		Subject: subject,
		TopicArn: this.snsTopicArn
	}, function(err) {
		if (err) self.emit('error', err);
	});
};

module.exports = Bus;
