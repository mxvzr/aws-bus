AWS powered message bus using SNS & SQS

# Usage

var bus = new Bus(ops);
bus.use(function(msg) {
	msg.start_time = Date.now();
})
.use(/[a-z]/, function(msg) {
	console.log(msg);
})
.use('red', function(msg) {
	message.red = JSON.parse(msg.Message);
})
.use('blue', function(msg) {
	var deferred = Q.defer();
	setTimeout(function() {
		deferred.resolve();
	}, 500);
	return deferred.promise();
})
.use('green', function(msg) {
	message.delete();
})
.use(function(msg) {
	console.log(Subject, Date.now() - msg.start_time);
})
.start();

bus.on('messages', function(messages) {
	if (messages.length)
		console.log(':)');
	else
		console.log(':(');
});

bus.on('error', function(err) {
	bus.stop();
});

bus.notify('red', {color: 'red'});
bus.notify('blue', 'check out my blue message');
bus.notify('green', 8000);

# API

Bus(opts) (inherits EventEmitter, see Events)
.length() -> Number
	Returns the number of messages currently being processed
.use(worker) -> Bus
	Add a message processing middleware
.use(path, worker) -> Bus
	Add a guarded message processing middleware. The path can be a string or
		regexp tested against the messages' subjects; or a function that receives a
		Message instance and returns a Boolean
.notify(subject, body)
	Send a message to the bus using SNS
.start()
.stop()

# Constructor options
{
	snsTopicArn: undefined // required to use .notify
	sqsQueueUrl: undefined // required to use .use && .start

	sqsMaxNumberOfMessages: 10 // max number of messages to pull at once
	sqsWaitTimeSeconds: 20 // long polling

	pollDelay: 0 ms // delay between 2 sqs receiveMessage calls
	defaultTick: 500 ms // interval between each poll check
	maxConcurrency: = sqsMaxNumberOfMessages

	sqsOpts: undefined // passed to SQS client constructor
	snsOpts: undefined // passed to SNS client constructor
}

# Message
Message
.Message
	The parsed message's body object
.Subject
	SNS Subject string
.delete()
	A function to delete the message
._msg
	The full SQS message object

# Events

"messages" -> [Message, ...] (empty array if poll returned no messages)
"error" -> Error
